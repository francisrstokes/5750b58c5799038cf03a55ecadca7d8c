import React from 'react';

export class StateDispatcher extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.state || {};
    this._dispatch = this.dispatch.bind(this);
  }

  dispatch(action) {
    this.setState(state => this.props.reducer(state, action));
  }

  getArgs() {
    return {
      state: this.state,
      dispatch: this._dispatch
    }
  }

  render() {
    const {children} = this.props;
    return children
      ? typeof children === 'function'
        ? children(this.getArgs())
        : React.createElement(children)
      : null;
  }
}
