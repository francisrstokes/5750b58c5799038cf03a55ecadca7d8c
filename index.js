import React from 'react';
import ReactDOM from 'react-dom';
import {StateDispatcher} from './StateDispatcher';

const initialState = {counter: 0};

const rootReducer = (state, action) => {
  switch (action.type) {
    case 'INC':
      return {...state, counter: state.counter + 1};
    case 'DEC':
      return {...state, counter: state.counter - 1};
    default:
      return state;
  }
};

ReactDOM.render(
  <StateDispatcher state={initialState} reducer={rootReducer}>
    {({state: {counter}, dispatch}) => {
      return <div>
        {counter}
        <br/>
        <button onClick={() => dispatch({type: 'INC'})}>+</button>
        <button onClick={() => dispatch({type: 'DEC'})}>-</button>
      </div>
    }}
  </StateDispatcher>,
  document.getElementById('root')
);
